<?php
$num = 1234;
$revnum = 0;
while ( $num != 0)
{
  $rem = $num % 10;
  $revnum = ($revnum * 10) + $rem;
  $num = (int)($num / 10);
}
echo "reverse number of 1234 is: $revnum";
?>