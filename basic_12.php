<?php

$x = 5;

$y = 6;
echo "before swapping number";
echo $x;
echo " ";
echo $y;
echo "\n";

$x = $x + $y;
$y = $x - $y;
$x = $x - $y;

echo "after swapping";
echo $x;
echo " ";
echo $y;
?>